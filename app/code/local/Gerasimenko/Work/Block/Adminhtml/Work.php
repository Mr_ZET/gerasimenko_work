<?php

class Gerasimenko_Work_Block_Adminhtml_Work extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_work';
        $this->_blockGroup = 'work';
        $this->_headerText = Mage::helper('work')->__('Work');
        parent::__construct();
    }
}