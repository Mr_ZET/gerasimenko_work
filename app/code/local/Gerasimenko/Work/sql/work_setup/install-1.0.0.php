<?php
/** @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;

$data = array(
    array(
        'link_type_id'  => Gerasimenko_Work_Model_Adminhtml_Catalog_Product_Link::LINK_TYPE_CUSTOM,
        'code'          => 'custom'
    )
);

foreach ($data as $bind) {
    $installer->getConnection()->insertForce($installer->getTable('catalog/product_link_type'), $bind);
}

/**
 * Install product link attributes
 */
$data = array(
    array(
        'link_type_id'                  => Inchoo_CustomLinkedProducts_Model_Catalog_Product_Link::LINK_TYPE_CUSTOM,
        'product_link_attribute_code'   => 'position',
        'data_type'                     => 'int'
    )
);

$installer->getConnection()->insertMultiple($installer->getTable('catalog/product_link_attribute'), $data);

//$installer->startSetup();
//
//$table = $installer->getConnection()
//    ->newTable($this->getTable('gerasimenko/work'))
//    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
//        'identity' => true,
//        'unsigned' => true,
//        'nullable' => false,
//        'primary' => true
//    ))
//    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
//        'nullable' => false
//    ));
//
//$installer->getConnection()->createTable($table);
//$installer->endSetup();
